%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script generates the eight shaped track for mbzirc competition
%
% Guilherme A. S. Pereira, January 2016
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
close all

radius=18-1.5; % Radius of the center of the road
center_circle=radius*sqrt(2);

disp(sprintf('      <point>0.00 0.00 0.1</point>'))
i=0;
for ang=pi/2+pi/4:-pi/50:-pi/2-pi/4;
    i=i+1;
    x(i)=radius*cos(ang)+(center_circle);
    y(i)=radius*sin(ang);
    disp(sprintf('      <point>%f %f 0.1</point>', x(i), y(i)))
end

i=0;
for ang=pi/2-pi/4:pi/50:pi+pi/2+pi/4;
    i=i+1;
    x(i)=radius*cos(ang)-(center_circle);
    y(i)=radius*sin(ang);
    disp(sprintf('      <point>%f %f 0.1</point>', x(i), y(i)))
end
disp(sprintf('      <point>0.00 0.00 0.1</point>'))