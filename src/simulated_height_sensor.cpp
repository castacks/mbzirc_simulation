#include <ros/ros.h>
#include <sensor_msgs/LaserScan.h>
#include <sensor_msgs/Range.h>

double current_height;

void sonarCallback(const sensor_msgs::RangeConstPtr &msg){
  current_height=msg->range;  
}


int main(int argc, char **argv) {

  ros::init(argc, argv, "height_sensor");
  ros::NodeHandle n;
  
  ros::Subscriber sub = n.subscribe("/sonar_height", 1, &sonarCallback);
  
  ros::Publisher laser_pub = n.advertise<sensor_msgs::LaserScan>("/height_sensor/range",1);
  
  ros::Rate loop_rate(10);
  
  ros::Time last_time=ros::Time::now();
  
  current_height=0.0;
  
  while (ros::ok())
     {
     
      ros::spinOnce();
     
     
      ros::Time now = ros::Time::now();
      ros::Duration duration=now-last_time;
      last_time=now;
      
      sensor_msgs::LaserScan data;
      data.header.frame_id = "sf30";
      data.header.stamp = ros::Time::now();
      data.scan_time =  duration.toSec();
      data.range_max=3.0;
      data.ranges.push_back(current_height);
      data.intensities.push_back(1.0); // Confidence = 0, range = 0 cannot be thrusted
      
      laser_pub.publish(data);
            
      loop_rate.sleep();  
     
     
     }
}
     