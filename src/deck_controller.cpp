
//******************************************************************//
//   Node that controls the deck to follow the eight shapped track
//   January 2016, Guilherme A. S. Pereira
//******************************************************************//

#include <ros/ros.h>
#include <signal.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <gazebo_msgs/ModelState.h>
#include <vector>
#include <math.h>
#include "tf/transform_datatypes.h"

// This is the callback for Pose
nav_msgs::Odometry curr_pose;
bool got_pose = false;
void GetOdom(const nav_msgs::Odometry::ConstPtr &msg) {
  curr_pose = *msg;
  got_pose = true;
}

double Distance(const nav_msgs::Odometry a, const nav_msgs::Odometry b) {

   return sqrt(pow(a.pose.pose.position.x-b.pose.pose.position.x, 2) + pow(a.pose.pose.position.y-b.pose.pose.position.y, 2) );
}


int main(int argc, char **argv) {

  ros::init(argc, argv, "deck_controller");
  ros::NodeHandle n("~");

  ros::Subscriber odometry_sub = n.subscribe<nav_msgs::Odometry>("odom", 1, GetOdom);
  ros::Publisher vel_pub = n.advertise<geometry_msgs::Twist>("cmd_vel", 1);
  ros::Publisher position_pub = n.advertise<gazebo_msgs::ModelState>("/gazebo/set_model_state", 1);

  double current_speed;
  int direction;

  n.param("/deck_controller/speed", current_speed, 4.1666);// 15Km/h
  n.param("/deck_controller/direction", direction, 1); // 1 or -1
  if (direction > 0) direction = 1;
  else direction =-1;
  if (current_speed < 0) current_speed *= -1;
  
  ros::Rate loop_rate(50);

  geometry_msgs::Twist deck_velocity; 
  
  // The track will be represented by a vector of waypoints
  std::vector<nav_msgs::Odometry> waypoints;
  nav_msgs::Odometry wp;
  wp.pose.pose.position.x=0;
  wp.pose.pose.position.y=0;
  wp.pose.pose.position.z=0;
  waypoints.push_back(wp);

  double radius=16.5;
  double circle_center=radius*sqrt(2);
  // Generates the right part of the eight 
  for (double ang=M_PI/2+M_PI/4; ang>=-(M_PI/2+M_PI/4); ang-=M_PI/80){
    wp.pose.pose.position.x=radius*cos(ang)+circle_center;
    wp.pose.pose.position.y=radius*sin(ang);  
    wp.pose.pose.position.z=0.0;
    waypoints.push_back(wp);
  }

  // Generates the left part of the eight -> 20 is the radius and 28.2843 is radius/cos(M_PI/4)
  for (double ang=M_PI/2-M_PI/4;ang<=M_PI+M_PI/2+M_PI/4; ang+=M_PI/80){
    wp.pose.pose.position.x=radius*cos(ang)-circle_center;
    wp.pose.pose.position.y=radius*sin(ang);  
    wp.pose.pose.position.z=0.0;
    waypoints.push_back(wp);
  }
  
  wp.pose.pose.position.x=0;
  wp.pose.pose.position.y=0;
  wp.pose.pose.position.z=0;
  waypoints.push_back(wp);

   // Wait for the initial pose
  while(!got_pose)
  {
      ros::spinOnce();
      loop_rate.sleep();
  }

  // Finds the closest waypoint to the deck
  int closest = 0;
  double min_dist = Distance(curr_pose, waypoints[0]); 
  for (int i=1; i<waypoints.size(); i++)
	if (Distance(curr_pose, waypoints[i])<min_dist){
		closest = i;
		min_dist=Distance(curr_pose, waypoints[closest]);      
	}
     
  
  

  double delta = 0.5; // This is what is considered close

  double dx, dy; 
  tf::Matrix3x3 m; 
  tf::Quaternion q;
  gazebo_msgs::ModelState deck_pose;
  bool changed=true;
   
  deck_pose.model_name="deck";
  deck_pose.pose=curr_pose.pose.pose;

  // Move the deck to the closest waypoint  
  deck_pose.pose=waypoints[closest].pose.pose;
  position_pub.publish(deck_pose);
  ros::spinOnce();
  loop_rate.sleep();

  
    
  // Main loop
  while (ros::ok())
     {
      
      ros::spinOnce();

      n.getParamCached("/deck_controller/direction", direction);
      if (direction < 0) direction = -1;
      else direction = 1;
      n.getParamCached("/deck_controller/speed", current_speed); 
      if (current_speed < 0) current_speed *= -1;
      if (current_speed > 8.33333) current_speed = 8.33333; // Saturate in 30km/h

      // Check if the deck arrived at the waypoint
      if (Distance(curr_pose, waypoints[closest])<delta){
          closest=closest+direction;
          if (closest<0) closest+=waypoints.size();
          closest=closest%waypoints.size();
          changed = true;
      }
      //ROS_INFO("Closest: %d, Distance: %f, Point: (%f, %f)", closest, Distance(curr_pose, waypoints[closest]), waypoints[closest].pose.pose.position.x, waypoints[closest].pose.pose.position.y);   
      //ROS_INFO("curr_pose: %f %f %f", curr_pose.pose.pose.position.x, curr_pose.pose.pose.position.y, curr_pose.pose.pose.position.z);
      
      dx = (waypoints[closest].pose.pose.position.x - curr_pose.pose.pose.position.x)/Distance(curr_pose, waypoints[closest]); 
      dy = (waypoints[closest].pose.pose.position.y - curr_pose.pose.pose.position.y)/Distance(curr_pose, waypoints[closest]);
      
      deck_pose.pose=curr_pose.pose.pose;
      
      // This is desired yaw      
      double d_yaw = atan2(dy, dx);
      
      // Create a quaternion from the desired yaw     
      m.setRPY(0.0, 0.0, d_yaw);
      m.getRotation(q);
      
      // Set the orientation to the desired quaternion
      tf::quaternionTFToMsg(q, deck_pose.pose.orientation);
        
      // Since the deck is pointing to the next waypoint, just move forward 
      deck_velocity.linear.x = current_speed; 
      deck_velocity.linear.y = 0;
      
      // If the deck arived at the waypoint, publish the orientation -> this was added to avoid unecessary messages
      if (changed){  
      	position_pub.publish(deck_pose);
	changed=false;
      }
      vel_pub.publish(deck_velocity);
      
      loop_rate.sleep();

    } // while

} // main
