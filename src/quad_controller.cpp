#include <ros/ros.h>
#include <signal.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <gazebo_msgs/ModelState.h>
#include <vector>
#include <math.h>
#include "tf/transform_datatypes.h"
#include <iostream>
#include <fstream>

// This is the callback for Pose
nav_msgs::Odometry curr_pose;
bool got_pose = false;
void GetOdom(const nav_msgs::Odometry::ConstPtr &msg) {
  curr_pose = *msg;
  got_pose = true;
}

// This is the callback for Deck Pose
nav_msgs::Odometry curr_deck_pose;
void GetDeckPose(const nav_msgs::Odometry::ConstPtr &msg) {
  curr_deck_pose = *msg;
}

// Distance in 3D
double Distance(const nav_msgs::Odometry a, const nav_msgs::Odometry b) {

   return sqrt(pow(a.pose.pose.position.x-b.pose.pose.position.x, 2) + pow(a.pose.pose.position.y-b.pose.pose.position.y, 2) + pow(a.pose.pose.position.z-b.pose.pose.position.z, 2));
}

// Distance in 2D
double Distance2(const nav_msgs::Odometry a, const nav_msgs::Odometry b) {

   return sqrt(pow(a.pose.pose.position.x-b.pose.pose.position.x, 2) + pow(a.pose.pose.position.y-b.pose.pose.position.y, 2) );
}

// Make the angles to be between -PI and PI
double fix_angle(const double ang){
   if (ang < -M_PI)
         return -M_PI/2-ang;
   else if (ang > M_PI)
 	 return M_PI/2-ang;
   else return ang;
}

// Error between to angles -> to avoid discontinuity
double ang_error(const double ang1, const double ang2){
   
   double error = fix_angle(ang1) - fix_angle(ang2);
  
   if (error > M_PI)
    	return error - 2*M_PI;
   else if (error < -M_PI)
   	return error + 2*M_PI;
   else
        return error;
	
}

int main(int argc, char **argv) {

  ros::init(argc, argv, "quad_controller");
  ros::NodeHandle n("~");

  ros::Subscriber odometry_sub = n.subscribe<nav_msgs::Odometry>("odom", 1, GetOdom);

  ros::Subscriber target_pos_sub = n.subscribe<nav_msgs::Odometry>("/deck/odom", 1, GetDeckPose);

  ros::Publisher vel_pub = n.advertise<geometry_msgs::Twist>("cmd_vel", 1);
  ros::Rate loop_rate(15);

  geometry_msgs::Twist quad_velocity; 
  
  

   // Wait for the initial pose
  while(!got_pose)
  {
      ros::spinOnce();
      loop_rate.sleep();
  }

  std::vector<nav_msgs::Odometry> waypoints;
  nav_msgs::Odometry wp;
  wp.pose.pose.position.x=0;
  wp.pose.pose.position.y=0;
  wp.pose.pose.position.z=5;
  waypoints.push_back(wp);

  double dx, dy, dz, d_yaw, roll, pitch, yaw; 
  //std::ofstream myfile;
  
  double max_speed = 5; // m/s 
  int state = 0; // 0 - take off; 1 - orient to the goal; 2 - move towards the goal; 3 - track the deck 

  while (ros::ok())
     {
      
      ros::spinOnce();

      if (state == 0){
   
        //ROS_INFO("State 0");

        quad_velocity.linear.x = 0.0; 
      	quad_velocity.linear.y = 0.0;
	quad_velocity.linear.z = 4;
	 
	if (curr_pose.pose.pose.position.z > 1)  // took off	
		state=1;

      }  
      else if (state == 1)
      {

        //ROS_INFO("State 1");
         
        quad_velocity.linear.x = 0.0; 
      	quad_velocity.linear.y = 0.0;
	quad_velocity.linear.z = 0.0;
        
        dx = (waypoints[0].pose.pose.position.x - curr_pose.pose.pose.position.x)/Distance(curr_pose, waypoints[0]); 
        dy = (waypoints[0].pose.pose.position.y - curr_pose.pose.pose.position.y)/Distance(curr_pose, waypoints[0]);

        d_yaw = atan2(dy, dx);

        tf::Quaternion q_current(curr_pose.pose.pose.orientation.x, curr_pose.pose.pose.orientation.y, curr_pose.pose.pose.orientation.z, curr_pose.pose.pose.orientation.w);
        tf::Matrix3x3 m_current(q_current);
        m_current.getRPY(roll,pitch,yaw);

        quad_velocity.angular.z= 0.5*ang_error(d_yaw,yaw);

        if (d_yaw-yaw<0.1) // oriented towards the waypoint
		state = 2; 
	 
      }     
      else if (state == 2)
      {

        ROS_INFO("State 2");        

       	dx = (waypoints[0].pose.pose.position.x - curr_pose.pose.pose.position.x)/Distance2(curr_pose, waypoints[0]); 
       	dy = (waypoints[0].pose.pose.position.y - curr_pose.pose.pose.position.y)/Distance2(curr_pose, waypoints[0]);
        dz = (waypoints[0].pose.pose.position.z - curr_pose.pose.pose.position.z);     
  
       	d_yaw = atan2(dy, dx);

       	tf::Quaternion q_current(curr_pose.pose.pose.orientation.x, curr_pose.pose.pose.orientation.y, curr_pose.pose.pose.orientation.z, curr_pose.pose.pose.orientation.w);
        tf::Matrix3x3 m_current(q_current);
       	m_current.getRPY(roll,pitch,yaw);

        quad_velocity.angular.z= 0.5*ang_error(d_yaw,yaw);
 
      	quad_velocity.linear.x = (0.5*Distance2(curr_pose, waypoints[0])>max_speed ? max_speed : 0.5*Distance2(curr_pose, waypoints[0]));
           
      	quad_velocity.linear.y = 0;
        quad_velocity.linear.z = 0.1*dz;     

      	if (Distance(curr_pose, waypoints[0])<1) // arrive at the destination
		state=3;   
 
      }
      else if (state == 3)
      {
         
        ROS_INFO("State 3");

        dx = (curr_deck_pose.pose.pose.position.x - curr_pose.pose.pose.position.x)/Distance2(curr_pose, curr_deck_pose); 
       	dy = (curr_deck_pose.pose.pose.position.y - curr_pose.pose.pose.position.y)/Distance2(curr_pose, curr_deck_pose);

        d_yaw = atan2(dy, dx);

       	tf::Quaternion q_current(curr_pose.pose.pose.orientation.x, curr_pose.pose.pose.orientation.y, curr_pose.pose.pose.orientation.z, curr_pose.pose.pose.orientation.w);
        tf::Matrix3x3 m_current(q_current);
       	m_current.getRPY(roll,pitch,yaw);

        //ROS_INFO("D_yaw: %f  Yaw: %f  Error: %f  Corrected error: %f", d_yaw, yaw, d_yaw-yaw, ang_error(d_yaw,yaw));
        quad_velocity.angular.z= 0.5*ang_error(d_yaw,yaw);

        quad_velocity.linear.x = (Distance2(curr_pose, curr_deck_pose)>max_speed ? max_speed :Distance2(curr_pose, curr_deck_pose));  
 
        //quad_velocity.linear.x = 0.0; 
        quad_velocity.linear.y = 0.0;
	quad_velocity.linear.z = 0.0;

     }


    vel_pub.publish(quad_velocity);
    //myfile.open ("/home/gpereira/target_trajectory.txt", std::ios::out | std::ios::app);
    //myfile  << curr_deck_pose.pose.pose.position.x << " " << curr_deck_pose.pose.pose.position.y << "\n";
    //myfile.close();
    loop_rate.sleep();
    }
}

