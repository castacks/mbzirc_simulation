//**********************************************************************************
// quad_tf_broadcaster - For hector simulation only - Guilherme Pereira, August 2016
//		
// world  ->  X North, Y West, Z Up 
// base_frame -> X front, Y right, Z Down (Aeronautics convention)
// camera -> Z orthogonal to the image pointing into the plane of the image, X points to right of the image (left of the camera), Y pointing down
//           This reference frame is defined following the standard in http://wiki.ros.org/image_pipeline/CameraInfo
// camera_visp -> Z orthogonal to the image pointing out of the image, X points to the left of the image (right of the camera), Y pointing down

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
//#include <dji_sdk/Gimbal.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <math.h>



int main(int argc, char** argv){
  ros::init(argc, argv, "dji_tf_broadcaster");

  ros::NodeHandle node;
  ros::Rate loop_rate(30);
  
  static tf::TransformBroadcaster br;
  static tf::TransformListener listener;
  tf::Transform transform;
  tf::StampedTransform stransform;
  tf::Quaternion q;
  ros::Time now;
  
  while(ros::ok()){  
    
     
     try{
        // Base_link to base_frame - this is a static transformation
	transform.setOrigin(tf::Vector3(0.0, 0.0, 0.0));
	q.setEuler(M_PI, 0, M_PI); 
	transform.setRotation(q);
	br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "base_link", "base_frame"));
         
	//TODO - IF WE HAVE A GIMBLE, THE FOLLOWING TRANSFORMATIONS SHOULD BE INSIDE A CALLBACK
        // World to camera_visp 
	now = ros::Time::now();
	listener.waitForTransform("world", "front_cam_optical_frame", now, ros::Duration(1.0));
	listener.lookupTransform("world", "front_cam_optical_frame", now, stransform); 
	transform.setOrigin(stransform.getOrigin());
	transform.setRotation(stransform.getRotation());
	br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "world", "camera_visp"));
	
	// camera_visp to camera - this is a static transformation
	transform.setOrigin(tf::Vector3(0.0, 0.0, 0.0));
	tf::Quaternion q;
	q.setEuler(M_PI, 0, 0); // Z points backards, X points right, Y points down - CameraInfo standard
	transform.setRotation(q);
	br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "camera_visp", "camera"));	
    }
    catch (tf::TransformException &ex) {
        //ROS_ERROR("%s",ex.what());
	ros::Duration(1.0).sleep();
    }
     
     ros::spinOnce();
     loop_rate.sleep();
  }
  return 0;
}
