**This package simulates the MBZIRC Competion Arena**

The simulation environment was tested using the ros-indigo-desktop-full only. To install this version of the ROS [follow this link](http://wiki.ros.org/indigo/Installation/Ubuntu). To install the simulation package along other MBZIRC packages follow the steps below.

If not installed, install wstool:


```
#!bash


sudo apt-get install python-wstool 
```


Create a workspace: 

```
#!bash

source /opt/ros/indigo/setup.bash # init environment
mkdir -p ~/mbzirc_ws/src
cd mbzirc_ws/src
catkin_init_workspace

```

Install the packages (We are installing most packages used in the MBZIRC project, not only the simulation). For these commands you need to set up SSH for your Git first. There is a good tutorial [here](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html).

```
#!bash

git clone git@bitbucket.org:castacks/metarepository.git
ln -s metarepository/rosinstall/mbzirc_desktop.rosinstall .rosinstall
wstool info # see status
wstool up # get stuff

```

Install dependencies:


```
#!bash

./mbzirc_simulation/install_dependencies.sh
./mbzirc_mission/install_dependencies.sh 
./ca_common/install_dependencies.sh
./mikrokopter/install_dependencies.sh

```



Compile:


```
#!bash


cd ..
catkin_make
```

There shouldn't be any errors due to non-compilation of messages. However, if errors come up, run `catkin_make` several times until there are no errors. If this is not sufficient, run `catkin_make --pkg name_of_the_package_with_missing_message` to force the message to be generated before it is needed. Please report the issue to the maintainers.


In a terminal run:

```
#!bash

source devel/setup.bash
roslaunch mbzirc_simulation arena.launch
```


In another terminal run: 


```
#!bash

source devel/setup.bash
roslaunch mbzirc_launch sim_planB.launch 
```


In another terminal:


```
#!bash

source devel/setup.bash
./src/mbzirc_launch/scripts/switch2auto.sh
```